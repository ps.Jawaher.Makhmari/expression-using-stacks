package com.progressoft.jip8;

import java.util.*;
public class evaluateExpressionUsingStacks {
    public static Double evaluate(String expression) {

        char[] tokens = expression.toCharArray();
        Stack<Double> operandStack = new Stack<Double>();
        Stack<Character> operationStack = new Stack<Character>();
        for (int i = 0; i < tokens.length; i++) {

            if (tokens[i] == ' ') {
                continue;
            }

            if (( tokens[i] >= '0' && tokens[i] <= '9' ) || ( tokens[i] >= 'a' && tokens[i] <= 'z' )) {
                StringBuffer a = new StringBuffer();

                while (i < tokens.length && ( ( tokens[i] >= '0' && tokens[i] <= '9' ) || ( tokens[i] >= 'a' && tokens[i] <= 'z' ) )) {


                    if (tokens[i] >= 'a' && tokens[i] <= 'z') {
                        Scanner sc = new Scanner(System.in);
                        double d = sc.nextDouble();
                        tokens[i] = (char) d;
                    }
                    a.append(tokens[i++]);
                }
                operandStack.push(Double.parseDouble(a.toString()));
            }


            else if (tokens[i] == '(')
                operationStack.push(tokens[i]);


            else if (tokens[i] == ')') {
                while (operationStack.peek() != '(')
                    operandStack.push(applyOp(operationStack.pop(), operandStack.pop(), operandStack.pop()));
                operationStack.pop();
            }


            else if (tokens[i] == '+' || tokens[i] == '-' ||
                    tokens[i] == '*' || tokens[i] == '/') {

                while (!operationStack.empty() && hasPrecedence(tokens[i], operationStack.peek()))
                    operandStack.push(applyOp(operationStack.pop(), operandStack.pop(), operandStack.pop()));


                operationStack.push(tokens[i]);
            }
        }

        while (!operationStack.empty())
            operandStack.push(applyOp(operationStack.pop(), operandStack.pop(), operandStack.pop()));

        return (Double) operandStack.pop();
    }

    public static boolean hasPrecedence(char op1, char op2) {
        if (op2 == '(' || op2 == ')')
            return false;
        if (( op1 == '*' || op1 == '/' ) && ( op2 == '+' || op2 == '-' ))
            return false;
        else
            return true;
    }

    public static Double applyOp(char op, Double b, Double a) {
        switch (op) {
            case '+':
                return a + b;
            case '-':
                return a - b;
            case '*':
                return a * b;
            case '/':
                if (b == 0)
                    throw new
                            UnsupportedOperationException("Cannot divide by zero");
                return a / b;
        }
        return 0.0;
    }
}